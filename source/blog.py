import feedparser
url = "http://varedatechin.blogfa.com/rss"



def return_recents(url):
    """
    returns a list of dictionaris that contantain the recent titles of articles and the corresponding link to the article.
    This list looks like: [{"title": "Linux Kernel", "link": "http://kernel.org/Post100"},]
    Argument: url, has to be a feed-url
    """
    
    feed = feedparser.parse(url)
    items = []
    counter = 0
    for item in feed["items"]:        
        items.append({"title": item["title"], "link": item["link"], "date": item["published"] })
        counter = counter 
    return items
    