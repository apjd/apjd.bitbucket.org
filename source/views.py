from flask import Flask, render_template, request, g
from blog import return_recents
from app import app

recent_posts = return_recents("http://varedatechin.blogfa.com/rss")

app.config.from_object('settings')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', recent_posts = recent_posts ), 404
    

@app.route('/')
def home():
   return render_template('home.html', recent_posts = recent_posts )
   
@app.route('/newsletter.html', methods=['POST'])
def newsletter():
    
    email_address = request.form['email_address']
    from apps import mail
    mail.send_mail(mail_content=email_address, mail_subject="[varedatechin.ir/newsletter]", mail_sender='news@varedatechin.ir')
    return render_template('newsletter.html', status = 'sent' )   
   
@app.route('/about.html')
def about():
    return render_template('about.html', recent_posts = recent_posts )
    
@app.route('/contact.html')
def contact():
    return render_template('contact.html', recent_posts = recent_posts )
   
@app.route('/sherkat.html')
def sherkati():
   return render_template('1.html', recent_posts = recent_posts )
   
@app.route('/gomroki.html')
def gomroki():
    return render_template('2.html', recent_posts = recent_posts )
    
@app.route('/ghavanin.html')
def ghavanin():
    return render_template('3.html', recent_posts = recent_posts )
    
@app.route('/arm.html')
def arm():
    return render_template('4.html', recent_posts = recent_posts )
    
@app.route('/vazayef.html')
def vazayef():
    return render_template('5.html', recent_posts = recent_posts )
    
@app.route('/gomrokat.html')
def gomrokat():
    return render_template('6.html', recent_posts = recent_posts )
    
@app.route('/etelaat.html')
def etelaat():
    return render_template('7.html', recent_posts = recent_posts )
    
@app.route('/arze_dolati.html')
def arze_dolati():
    return render_template('8.html', recent_posts = recent_posts )
    
@app.route('/arze_mobadelati.html')
def arze_mobaledati():
    return render_template('9.html', recent_posts = recent_posts )
    
@app.route('/tarkhis.html')
def tarkhis():
    return render_template('10.html', recent_posts = recent_posts )
    
@app.route('/havalejat.html')
def havalejat():
    return render_template('11.html', recent_posts = recent_posts )
    
@app.route('/kharid.html')
def kharid():
    return render_template('12.html', recent_posts = recent_posts )
    
@app.route('/sabt.html')
def sabt():
    return render_template('13.html', recent_posts = recent_posts )
    
@app.route('/zemanat.html')
def zemanat():
    return render_template('14.html', recent_posts = recent_posts )
    
@app.route('/moshavere.html')
def moshavere():
    return render_template('15.html', recent_posts = recent_posts )
   
