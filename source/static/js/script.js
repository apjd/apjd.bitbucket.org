var active = 1, count = 0, iv, imageWidth = 950;

function showThis(obj)
{
	$(".parent").removeClass('pselected');
	$(obj).addClass('pselected');
	$('.child-list').hide();
	$(obj).parent().find('.child-list').show();
	return false;
}
function setColor(c, obj)
{
	$('.clrs a').removeClass('active');
	$(obj).addClass('active');
	$('input[name="fld_color"]').val(c);
	return false;
}

$(document).ready( function() 
{
	iv = setInterval('moveIt(1)', 5000);
	$('a.prev').click(function(e){e.preventDefault();moveIt(-1);clearInterval(iv);});
	$('a.next').click(function(e){e.preventDefault();moveIt(1);clearInterval(iv);});
});


function moveIt(p)
{
	if (count == 0)
		count = $('#slide-items img').length;
		
		
	if (p > 0)
	{
		active++;
		if (active > count)
			active = 1;
		
		
		var nP = active * imageWidth - imageWidth;
	}
	else if (p < 0)
	{
		active--;
		if (active <= 0)
			active = count;
		
		
		var nP = active * imageWidth - imageWidth;
	}
	$("#slide-items").animate({left: '-' + nP + 'px'}, 'normal', 'easeOutCirc');
}